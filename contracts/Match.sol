pragma solidity ^0.4.23;

import "./SafeMath.sol";
import "./PullPayment.sol";

/**
* @title Match.
* @author Gorilla Logic, Inc.
* @dev Smart Contract to run soccer match betting.
*/
contract Match is PullPayment {
    using SafeMath for uint256;

    uint256 constant BET_PRICE = 5 finney;  // 0.005 ETH
    uint256 constant POT_PORTION = 4500 szabo; // 0.0045 ETH
    uint256 constant ORGANIZER_PORTION = 500 szabo; // 0.0005 ETH
    uint8 constant MAX_SCORE = 15;
    uint256 constant WORLD_CUP_END = 1531670400; // July 15, 2018 4:00 PM (GMT)
    
    event LogExcessSentFailed(address sentTo, uint amount);
    event LogPredictionSubmitted(address participant, uint pot);
    event LogGameStarted();
    event LogGameEnded(uint8 scoreTeamA, uint8 scoreTeamB);
    event LogPrizeClaimed(address winner);

    /** 
     * @title Status.
     * @dev Represents the game status.
     */
    enum Status {
        NotStarted,     // 0 previous to game start
        InProgress,     // 1 game has started
        Ended,          // 2 game has ended - official score sent by admin
        Completed       // 3 World Cup ends - organizer can claim winnings
    }

    /** 
     * @title Prediction.
     * @dev Represents a participant's prediction about this match.
     * A participant can only provide one prediction per match.
     */
    struct Prediction{
        bool predicted;
        uint8 teamAScore;
        uint8 teamBScore;
    }

    // **** Contract Owner - Administrator - Organizer ****
    address public admin;

    // **** Match information ****
    string public teamA;
    string public teamB;
    uint public kickOffTime;
    Status public currentStatus;  // Game status only set by admin.
    uint8 private teamAScore; // Official score of local team (admin only).
    uint8 private teamBScore; // Official score of visitor team (admin only).

    // **** Betting pools balances ****
    uint256 public potPool = 0; // Total bet pot to be claimed by winners
    uint256 public organizerPool = 0; // Total winnings for the organizer
    uint256 public prize = 0; // Calculated prize set by contract at game end
    mapping(address => bool) private claimed; // Control prize claiming
    mapping(uint8 => address[]) private scoresMap; // Maps scores to addresses
    // **** Predictions ****
    mapping(address => Prediction) public predictions;
    
    uint64 public totalPredictions = 0; // Total valid predictions submitted 

    /**
    * @dev Throws if called by any account other than the admin.
    */
    modifier adminOnly() {
        require(
            msg.sender == admin, 
            "Only contract admin can call this function."
        );
        _;
    }

    /**
    * @dev Throws if called by admin account.
    */
    modifier notAdmin() {
        require(
            msg.sender != admin, 
            "Contract admin cannot call this function."
        );
        _;
    }

    /**
    * @dev Throws if the current status does not match the require status.
    */
    modifier atStatus(Status _status) {
        require(
            currentStatus == _status,
            "Function cannot be called at this stage"
        );
        _;
    }

    /**
    * @dev Throws if called before a given timestamp.
    */
    modifier onlyAfter(uint _time) {
        require(
            now >= _time,
            "Function cannot be called at this time!"
        );
        _;
    }

    /**
    * @dev Throws if called after a given timestamp.
    */
    modifier onlyBefore(uint _time) {
        require(
            now <= _time,
            "Function cannot be called at this time!"
        );
        _;
    }

    /**
     * @dev Require an exact amount of ETH if the caller sent too much
     * he or she is refunded after the function executes.
     */
    modifier costs(uint _amount) {
        require(msg.value >= _amount);
        _;
        if (msg.value > _amount)
            if (!msg.sender.send(msg.value - _amount))
                emit LogExcessSentFailed(msg.sender, msg.value - _amount);
    }

    /** 
     * @dev Creates a new Match contract with the given teams.
     * @param _teamA The name of the local team for the match.
     * @param _teamB The name of the visitor team for the match.
     * @param _kickOffTime Unix timestamp for the game kick off.
     */
    constructor(string _teamA, string _teamB, uint _kickOffTime) public {
        admin = msg.sender;
        teamA = _teamA;
        teamB = _teamB;
        kickOffTime = _kickOffTime;
        currentStatus = Status.NotStarted;
    }

    /**
     * @dev Gets the index associated with a given match score.
     * Converts the given score into an positive integer index.
     * So it distributes valid score predictions into a set of
     * different indices so each score gets a different index.
     * This allows us to group the same score in the same slot but
     * different scores into different slots.
     * @param _teamAScore Number of goals of team A (local team).
     * @param _teamBScore Number of goals of team B (visitor team).
     * @return Calculated index for the given score.
     */
    function getScoreIndex(uint8 _teamAScore, uint8 _teamBScore)
        private
        pure
        returns (uint8)
    {
        uint8 scoreIndex = _teamAScore*100 - _teamBScore + MAX_SCORE*10;
        return scoreIndex;
    }

    /**
     * @dev Called by the admin to indicate the game has just started.
     * Can't be called before the actual time scheduled for the game. (approx)
     */
    function gameStarted() 
        public
        adminOnly
        atStatus(Status.NotStarted)
        onlyAfter(kickOffTime)
    {
        currentStatus = Status.InProgress;
        emit LogGameStarted();
    }

    /**
     * @dev Called by the admin to indicate the end of the match.
     * It sets the official score of the game by the contract admin.
     * Can't be called before the actual time scheduled for game end. (approx)
     * @param _teamAScore Official number of goals of team A.
     * @param _teamBScore Official number of goals of team B.
     */
    function gameEnded(uint8 _teamAScore, uint8 _teamBScore) 
        public
        adminOnly
        atStatus(Status.InProgress) 
    {
        // @todo: should test timestamp here
        currentStatus = Status.Ended;
        teamAScore = _teamAScore;
        teamBScore = _teamBScore;
        calculatePrize();
        rewardWinners();
        emit LogGameEnded(teamAScore, teamBScore);
    }

    /**
     * @dev Calculates final prize by dividing the pot pool between the
     * total number of winners.
     */
    function calculatePrize() 
        private
        adminOnly 
        atStatus(Status.Ended) 
    {
        uint8 winnersScoreIndex = getScoreIndex(teamAScore,teamBScore);
        uint256 numberOfWinners = scoresMap[winnersScoreIndex].length;
        if (numberOfWinners > 0) {
            prize = potPool.div(numberOfWinners);
        }
    }

    /**
     * @dev Distribute prize among winners!
     */
    function rewardWinners()
        private
        adminOnly
        atStatus(Status.Ended)
    {
        // Only if there are actual winners
        if (prize > 0) {
            uint8 winnersScoreIndex = getScoreIndex(teamAScore,teamBScore);
            address[] storage winners = scoresMap[winnersScoreIndex];
            uint numberOfWinners = winners.length;
            // Verify balance makes sense
            assert(address(this).balance > numberOfWinners * POT_PORTION);        
            for (uint i=0; i < numberOfWinners; i++)
            {
                address winner = winners[i];
                Prediction storage prediction = predictions[winner];
                // Verify its an actual winner
                assert(
                    (teamAScore == prediction.teamAScore) &&
                    (teamBScore == prediction.teamBScore)
                );
                // @todo: emit event to congratulate winner
                asyncSend(winner, prize);
            }
        }
    }


    /**
     * @dev Submit a prediction for this match. Only approved participants
     * can send their predictions.
     * This function can't be called by the contract administrator
     * @param _teamAScore Goals the team A will score.
     * @param _teamBScore Goals the team B will score.
     */
    function submitPrediction(uint8 _teamAScore, uint8 _teamBScore) 
        public
        payable
        notAdmin
        atStatus(Status.NotStarted)
        onlyBefore(kickOffTime)
        costs(BET_PRICE)
    {
        // Already submitted a prediction?
        require(
            !predictions[msg.sender].predicted,
            "Participant already submitted a prediction for this match"
        );
        // Test maximum score allowed
        require(
            _teamAScore <= MAX_SCORE && _teamBScore <= MAX_SCORE,
            "Violated maximum score prediction"
        );

        predictions[msg.sender].predicted = true;
        predictions[msg.sender].teamAScore = _teamAScore;
        predictions[msg.sender].teamBScore = _teamBScore;

        uint8 scoreIndex = getScoreIndex(_teamAScore, _teamBScore);
        scoresMap[scoreIndex].push(msg.sender);
        totalPredictions += 1;

        potPool = potPool.add(POT_PORTION);
        organizerPool = organizerPool.add(ORGANIZER_PORTION);

        emit LogPredictionSubmitted(msg.sender, potPool);

        assert(address(this).balance >= BET_PRICE * totalPredictions);
    }


    /**
     * @dev A participant claiming his or her prize!
     * Not really needed as participant could call 
     * withdrawPayments() directly but used on the front-end
     * to enforce more control.
     */
    function claimPrize()
        public
        notAdmin
        atStatus(Status.Ended)
    {
        // Already claimed?
        require(
            !claimed[msg.sender],
            "Prize already claimed!"
        );

        if (prize > 0) {
            claimed[msg.sender] = true;
            potPool = potPool.sub(prize);
            withdrawPayments();
            emit LogPrizeClaimed(msg.sender);
        }
    }

    /**
     * @dev Called by the admin to indicate the end of the world cup.
     * Can't be called before the actual end of the World Cup (approx).
     * After calling this function the admin will be allowed to withdraw
     * profits made and remaining balances.
     */
    function worldCupEnds() 
        public
        adminOnly
        atStatus(Status.Ended)
        onlyAfter(WORLD_CUP_END)
    {
        currentStatus = Status.Completed;
    }

    /**
     * @dev Called by the admin to withdraw profits and remaining balances.
     * Could only be called after the worldCupEnds function succeed.
     * This means a two step verification process so participants can be
     * confident the owner of the contract will not claim balances before
     * they can access their possible prizes.
     */
    function claimProfit()
        public
        adminOnly
        atStatus(Status.Completed)
        onlyAfter(WORLD_CUP_END)
    {
        // Verify numbers makes sense
        assert(organizerPool == (ORGANIZER_PORTION * totalPredictions)); 
        // Verify balance makes sense
        assert(address(this).balance >= organizerPool); 

        admin.transfer(address(this).balance);
    }
}