# Quiniela

The project will be a small sports betting web app that runs over the Ethereum network using Smart Contracts. It will allow participants to make bets on scores of World Cup official matches using ETH on the Rinkeby test network.

This repository is intended to store the code for the Solidity Smart Contracts.